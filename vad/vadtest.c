#include "stdio.h"   
#include "wb_vad.h"
/* Use the newer ALSA API */
#define ALSA_PCM_NEW_HW_PARAMS_API
#include <alsa/asoundlib.h>
#include <stdlib.h>

#if 0
void main(int argc,char* argv[])   
{      
	if(argc != 4){
	   printf("usage: alsa_vad pcm_device output record_period_seconds\nfor example: ./alsa_vad hw:1,0 out 10\n");
	   return;
	}

	char* pcm_device_name = argv[1];
	char* output_file_name = argv[2];
	int   record_seconds = atoi(argv[3]);
	// for alsa
	long loops;
	int rc;
	int size;
	int size_one_channel;
	snd_pcm_t *handle;
	snd_pcm_hw_params_t *params;
	int dir;
	char *buffer;
	char *buffer_one_channel;
	int i;

	snd_pcm_stream_t  stream = SND_PCM_STREAM_CAPTURE;
	snd_pcm_access_t  mode = SND_PCM_ACCESS_RW_INTERLEAVED;
	snd_pcm_format_t  format = SND_PCM_FORMAT_U16_LE;
	unsigned int      channels = 2;
	unsigned int      rate     = 16000;
	snd_pcm_uframes_t frames   = FRAME_LEN;
	int bit_per_sample,bit_per_frame,chunk_bytes;

	//for vda
	float indata[FRAME_LEN];   
	short outdata[FRAME_LEN];   
	VadVars *vadstate;                     
	char name[128]; 
	int temp,vad;
	int recording = 0;
	int count = 0;
	FILE *fp = NULL;
	FILE *fp_all = NULL;
	// alsa init
	//rc = snd_pcm_open(&handle, pcm_device_name,stream, 0);
	rc = snd_pcm_open(&handle, "default",SND_PCM_STREAM_CAPTURE, 0);
	if (rc < 0) {
	    fprintf(stderr,"unable to open pcm device: %s\n",snd_strerror(rc));
	    exit(1);
	}
	snd_pcm_hw_params_alloca(&params);
	snd_pcm_hw_params_any(handle, params);
	snd_pcm_hw_params_set_access(handle, params,mode);
	snd_pcm_hw_params_set_format(handle, params,format);
	snd_pcm_hw_params_set_channels(handle, params, channels);
	snd_pcm_hw_params_set_rate_near(handle, params,&rate, 0);
	snd_pcm_hw_params_set_period_size_near(handle,params, &frames, 0);
	rc = snd_pcm_hw_params(handle, params);
	if (rc < 0) {
	    fprintf(stderr,"unable to set hw parameters: %s\n",snd_strerror(rc));
	    exit(1);
	}
	snd_pcm_hw_params_get_period_size(params,&frames, 0);
	size = frames * 4; /* 2 bytes/sample, 2 channels */
	size_one_channel = frames * 2;
	buffer = (char*) malloc(size);
	buffer_one_channel = (char*) malloc(size_one_channel);

	//vad init
	wb_vad_init(&(vadstate));
	sprintf(name,"%s.pcm",output_file_name);
	fp_all = fopen(name,"wb");

	/* We want to loop for 5 seconds */
	snd_pcm_hw_params_get_period_time(params,&rate, &dir);
	loops = record_seconds * 1000000 / rate;

	while (loops > 0) {
	    loops--;
	    rc = snd_pcm_readi(handle, buffer, frames);
	    if (rc == -EPIPE) {
	      /* EPIPE means overrun */
	      printf(stderr, "overrun occurred\n");
	      snd_pcm_prepare(handle);
	    } else if (rc < 0) {
	      printf(stderr, "error from read: %s\n",snd_strerror(rc));
	    } else if (rc != (int)frames) {
	      printf(stderr, "short read, read %d frames\n", rc);
	    }
#if 0
	    for(i = 0; i< frames; i++)
	    {
		indata[i]=0;
		temp = 0;
		memcpy(&temp,buffer+4*i,2);
		indata[i]=(float)temp;
		outdata[i]=temp;
		if(indata[i]>65535/2)
		    indata[i]=indata[i]-65536;
	    }
	    vad=wb_vad(vadstate,indata);
	    if(vad == 1)
	       	recording = 1;
	    else
		recording = 0;
	    if(recording ==1 && fp == NULL)
	    {
		sprintf(name,"%s.%d.pcm",output_file_name,count);
		fp=fopen(name,"wb");   
	    }
	    if(recording == 0 && fp != NULL)
	    {   
		fclose(fp);
		fp = NULL;
		count++;
	    }
#endif
//	    if(fp != NULL && recording == 1)
//		fwrite(outdata,2,FRAME_LEN,fp);
	    if(fp_all != NULL)
		fwrite(outdata,2,FRAME_LEN,fp_all);
	}
    fcloseall();
}
#endif


/* 
This example reads from the default PCM device 
and writes to standard output for 5 seconds of data. 
*/  
/* Use the newer ALSA API */  
#include <stdio.h>
#define ALSA_PCM_NEW_HW_PARAMS_API  
#include <alsa/asoundlib.h>  
int main() {  
	long loops;  
	int rc,i = 0;  
	int size;  
	FILE *fp = NULL ;
	snd_pcm_t *handle;  
	snd_pcm_hw_params_t *params;  
	unsigned int val,val2;  
	int dir;  
	snd_pcm_uframes_t frames;  
	FILE *fp_all = NULL;
	char *buffer;  
	if(  (fp_all =fopen("sound.wav","w")) < 0)
		printf("open sound.wav fial\n");

	/* Open PCM device for recording (capture). */  
	rc = snd_pcm_open(&handle, "default",  
	SND_PCM_STREAM_CAPTURE, 0);  
	if (rc < 0) {  
		fprintf(stderr,  "unable to open pcm device: %s/n",  snd_strerror(rc));  
		exit(1);  
	}  
	/* Allocate a hardware parameters object. */  
	snd_pcm_hw_params_alloca(&params);  
	/* Fill it in with default values. */  
	snd_pcm_hw_params_any(handle, params);  
	/* Set the desired hardware parameters. */  
	/* Interleaved mode */  
	snd_pcm_hw_params_set_access(handle, params,  
	SND_PCM_ACCESS_RW_INTERLEAVED);  
	/* Signed 16-bit little-endian format */  
	snd_pcm_hw_params_set_format(handle, params,  
	SND_PCM_FORMAT_S16_LE);  
	/* Two channels (stereo) */  
	snd_pcm_hw_params_set_channels(handle, params, 2);  
	/* 44100 bits/second sampling rate (CD quality) */  
	val = 16000;  
	snd_pcm_hw_params_set_rate_near(handle, params,  &val, &dir);  
	/* Set period size to 32 frames. */  
	frames = 256;  
	snd_pcm_hw_params_set_period_size_near(handle,  params, &frames, &dir);  
	/* Write the parameters to the driver */  
	rc = snd_pcm_hw_params(handle, params);  
	if (rc < 0) {  
		fprintf(stderr,  "unable to set hw parameters: %s/n",  
		snd_strerror(rc));  
		exit(1);  
	}  
	/* Use a buffer large enough to hold one period */  
	snd_pcm_hw_params_get_period_size(params,  &frames, &dir);  
	size = frames * 4; /* 2 bytes/sample, 2 channels */  
	printf("size = %d\n",size);
	buffer = (char *) malloc(size);  
	/* We want to loop for 5 seconds */  
	snd_pcm_hw_params_get_period_time(params,  &val, &dir);  
	loops = 10000000 / val; 

	float indata[FRAME_LEN];   
	short outdata[FRAME_LEN];  
	int temp,vad;
	int recording = 0;
 	VadVars *vadstate;  
	wb_vad_init(&(vadstate));
	char name[128]; 
	int count = 0;


#if 1
	while (1) {  
		//loops--;  
		rc = snd_pcm_readi(handle, buffer, frames); 
//		printf("%d\n",i++); 
		if (rc == -EPIPE) {  
		/* EPIPE means overrun */  
		
		snd_pcm_prepare(handle);  
		} else if (rc < 0) {  
		
		} else if (rc != (int)frames) {  
		fprintf(stderr, "short read, read %d frames/n", rc);  
		}  

#if 1
	    for(i = 0; i< frames; i++)
	    {
		indata[i]=0;
		temp = 0;
		memcpy(&temp,buffer+4*i,2);
		indata[i]=(float)temp;
		outdata[i]=temp;
		if(indata[i]>65535/2)
		    indata[i]=indata[i]-65536;
	    }
	    vad=wb_vad(vadstate,indata);
	    if(vad == 1)
	       	recording = 1;
	    else
		recording = 0;
		printf("recording==%d\n",recording);
	    if(recording ==1 && fp == NULL)
	    {
		sprintf(name,"out.%d.pcm",count);
		printf("create\n");
		fp=fopen(name,"wb");   
	    }
	    if(recording == 0 && fp != NULL)
	    {   
		fclose(fp);
		fp = NULL;
		printf("nodata\n");
		count++;
	    }
#endif
	    if(fp != NULL && recording == 1)
       		 fwrite(outdata,2,frames,fp);
	    rc = fwrite( outdata,2, frames,fp_all);  
		//rc = write(fp,buffer,size);
		if (rc != size)  
			;//fprintf(stderr,  "short write: wrote %d bytes\n", rc);  
		else 
			;//printf("fwrite buffer success\n");
	}  
#endif
	/******************打印参数*********************/
#if 0
	snd_pcm_hw_params_get_channels(params, &val);  
	printf("channels = %d\n", val);  
	snd_pcm_hw_params_get_rate(params, &val, &dir);  
	printf("rate = %d bps\n", val);  
	snd_pcm_hw_params_get_period_time(params,  
	&val, &dir);  
	printf("period time = %d us\n", val);  
	snd_pcm_hw_params_get_period_size(params,  
	&frames, &dir);  
	printf("period size = %d frames\n", (int)frames);  
	snd_pcm_hw_params_get_buffer_time(params,  
	&val, &dir);  
	printf("buffer time = %d us\n", val);  
	snd_pcm_hw_params_get_buffer_size(params,  
	(snd_pcm_uframes_t *) &val);  
	printf("buffer size = %d frames\n", val);  
	snd_pcm_hw_params_get_periods(params, &val, &dir);  
	printf("periods per buffer = %d frames\n", val);  
	snd_pcm_hw_params_get_rate_numden(params,  
	&val, &val2);  
	printf("exact rate = %d/%d bps\n", val, val2);  
	val = snd_pcm_hw_params_get_sbits(params);  
	printf("significant bits = %d\n", val);  
	//snd_pcm_hw_params_get_tick_time(params,  &val, &dir);  
	printf("tick time = %d us\n", val);  
	val = snd_pcm_hw_params_is_batch(params);  
	printf("is batch = %d\n", val);  
	val = snd_pcm_hw_params_is_block_transfer(params);  
	printf("is block transfer = %d\n", val);  
	val = snd_pcm_hw_params_is_double(params);  
	printf("is double = %d\n", val);  
	val = snd_pcm_hw_params_is_half_duplex(params);  
	printf("is half duplex = %d\n", val);  
	val = snd_pcm_hw_params_is_joint_duplex(params);  
	printf("is joint duplex = %d\n", val);  
	val = snd_pcm_hw_params_can_overrange(params);  
	printf("can overrange = %d\n", val);  
	val = snd_pcm_hw_params_can_mmap_sample_resolution(params);  
	printf("can mmap = %d\n", val);  
	val = snd_pcm_hw_params_can_pause(params);  
	printf("can pause = %d\n", val);  
	val = snd_pcm_hw_params_can_resume(params);  
	printf("can resume = %d\n", val);  
	val = snd_pcm_hw_params_can_sync_start(params);  
	printf("can sync start = %d\n", val);  
	/*******************************************************************/
#endif
	snd_pcm_drain(handle);  
	snd_pcm_close(handle); 
	fclose(fp_all); 
	free(buffer);  
	return 0;

}
